public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal(4, "Green");
        Dogs dog = new Dogs(4, "Blue");
        dog.sounding();
        animal.sounding();
        Animal animalDog = new Dogs(4, "Purple");
        animalDog.sounding();
        //  Không thể gọi được animal.numberLeg vì nó có access private
        //  Không thể sử dụng animal.getNumberLeg vì nó không có setter
        //  Không thể thay đổi được giá trị của numberLeg vì nó không có setter
    }
}