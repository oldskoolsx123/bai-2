public class Dogs extends Animal{

    public Dogs(int numberLeg, String color) {
        super(numberLeg, color);
        super.sounding();
    }
    @Override
    public void sounding(){
        System.out.println("Gâu Gâu Gâu");
    }
    public void sounding(String barkingSound){
        System.out.println(barkingSound);
    }
}
